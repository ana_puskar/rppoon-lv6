using System;

namespace RPPOON___LV6__2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();

            box.AddProduct(new Product("Water bottle", 89.99));
            box.AddProduct(new Product("Painting", 349.99));
            box.AddProduct(new Product("Sunglasses", 49.89));

            Iterator iterator = (Iterator)box.GetIterator();
            Console.WriteLine("Box contents:\n");
            for(Product product = iterator.First(); iterator.IsDone == false; product = iterator.Next())
            {
                Console.WriteLine(product.ToString());
            }
        }
    }
}