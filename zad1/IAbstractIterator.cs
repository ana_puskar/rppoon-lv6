using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV6___AP
{
    interface IAbstractIterator
    {
        Note First();
        Note Next();
        bool IsDone { get; }
        Note Current { get; }
    }
}