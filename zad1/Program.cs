using System;

namespace RPPOON___LV6___AP
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            
            notebook.AddNote(new Note("Podsjetnik", "Kolokvij u 8:00"));
            notebook.AddNote(new Note("Važno", "Prezentacija iz praktikuma do 12:00"));
            notebook.AddNote(new Note("Pjesma za poslušat", "Brye - Lemons"));

            Iterator iterator = (Iterator)notebook.GetIterator();

            for(Note note = iterator.First(); iterator.IsDone == false; note = iterator.Next())
            {
                note.Show();
            }
        }
    }
}